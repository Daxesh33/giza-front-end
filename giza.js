let Application = PIXI.Application,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    Sprite = PIXI.Sprite;
Rectangle = PIXI.Rectangle;
Text = PIXI.Text;
TextStyle = PIXI.TextStyle;

let app = new Application({
    width: 1200,
    height: 600,
    antialias: true,
    transparent: false,
    resolution: 1,
});
let tweenArray = [];
let scale1 = 1,
    i = 0,
    min = 1,
    max = 1.2,
    limit = 300;
let temp = 0,
    width1 = 0,
    height = 0,
    spriteArrray = [];
mainSpritePosition = 0;
let texArray = [
    "images/pog/h1.png",
    "images/pog/h2.png",
    "images/pog/h3.png",
    "images/pog/h4.png",
    "images/pog/h5.png",
    "images/pog/h6.png",
    "images/pog/low1.png",
    "images/pog/low2.png",
    "images/pog/low3.png",
    "images/pog/low4.png"
];
document.body.appendChild(app.view);

loader.add([
    "images/pog/h1.png",
    "images/pog/h2.png",
    "images/pog/h3.png",
    "images/pog/h4.png",
    "images/pog/h5.png",
    "images/pog/h6.png",
    "images/pog/low1.png",
    "images/pog/low2.png",
    "images/pog/low3.png",
    "images/pog/low4.png",
    "images/pog/spin.png"
]).load(setup);

let mainContainer = new PIXI.Container();
let wheelsSprites = {};

app.stage.addChild(mainContainer);
/*obj for map the array
 *array from backend
 */

let reel = {
    0: "images/pog/h1.png",
    1: "images/pog/h2.png",
    2: "images/pog/h3.png",
    3: "images/pog/h4.png",
    4: "images/pog/h5.png",
    5: "images/pog/h6.png",
    6: "images/pog/low1.png",
    7: "images/pog/low2.png",
    8: "images/pog/low3.png",
    9: "images/pog/low4.png",
};

let textArrayB = {};

/*function for map the backend responsed array
 */
let a = {
    "reel0": [1, 2, 2, 4],
    "reel1": [2, 3, 2, 4],
    "reel2": [2, 8, 2, 4],
    "reel3": [8, 8, 8, 4],
    "reel4": [0, 0, 0, 4]
}

function mapArray(array) {
    Object.keys(array).forEach((reels) => {
        textArrayB[reels] = [];

        array[reels].forEach((num) => {
            textArrayB[reels].push(reel[num]);
        });
    });
}


function randomNo(min, max) {
    return Math.floor(Math.random() * (+max - +min)) + +min;
}

function changePosition(sprite) {
    sprite.y = 300 * temp;
    temp++;
}

function changePositionMainSprite(sprite) {
    sprite.position.set(180 * width1, 150);
    width1++;
}
let myArr = [];

function setup() {
    // mapArray(a);

    firstView(0);
    firstView(1);
    firstView(2);
    firstView(3);
    firstView(4);

    /*to crop the container in rectangle
     */
    let mainSpriteCrop = new PIXI.Graphics();
    mainSpriteCrop.drawRect(0, 150, 1950, 890);
    mainSpriteCrop.renderable = true;
    mainContainer.addChild(mainSpriteCrop);
    mainContainer.mask = mainSpriteCrop;
    mainContainer.scale.set(0.5);



    app.ticker.add(delta => tween());

    const button = new Sprite(PIXI.utils.TextureCache["images/pog/spin.png"]);

    button.anchor.set(0.5);
    button.scale.set(0.5);
    button.buttonMode = true;
    button.interactive = true;
    button.position.set(860, 565);
    button.on('mousedown', start);
    app.stage.addChild(button);

    // app.ticker.add(delta => wheelsArray[1].animation(1, 1));
    // app.ticker.add(delta => wheelsArray[2].animation(2, 1));
}
let spin1 = 0;

function start() {
    spin1++;
    if (spin1 === 1) {
        app.ticker.add(delta => spin(delta));
    }
    responseBackend();
    winAnimation = false;
    tweenArray = [];
    wheelsArray.forEach((wheel) => {
        wheel.start();
    });

}

function responseBackend() {
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            myArr = JSON.parse(this.responseText);
            mapArray(myArr);
        }
    };
    xhttp.open("GET", "http://b83eb9b5.ngrok.io/reelspin", true);
    xhttp.send();
    stopTime(0, 1000);
    stopTime(1, 1500);
    stopTime(2, 2000);
    stopTime(3, 2500);
    stopTime(4, 3000);
}

const wheelsArray = [];

function firstView(keyname) {
    let mainSprite = new Sprite();
    let wheelInstance = new wheel(keyname);
    wheelsArray.push(wheelInstance);
    for (let i = 0; i < 4; i++) {
        const texture = PIXI.utils.TextureCache[texArray[randomNo(0, 9)]];
        const data = wheelInstance.add(texture);
        wheelInstance.sprites.push(data);
        mainSprite.addChild(data);
        wheelInstance.changePosition(data);
    }
    mainSprite.position.set(400 * mainSpritePosition, 0)
    mainSpritePosition++;
    temp = 0;
    wheelInstance.wheelSprite = mainSprite;
    mainContainer.addChild(mainSprite);
}

const wheel = function(i) {
    this.isStopped = false;
    this.isFinishing = false;
    this.finishAt = null;
    this.index = i;
    this.shifted = 0;
    this.count = 0;
    this.texString = '';
    this.data = null;
    this.sprites = [];
    this.wheelSprite = null;
    this.symbols = [];
}

function addTween(sprite) {
    const data = {
        sprite: sprite,
        isStopped: false,
        timeout: 2000,
        current: 1,
        max: 1,
        min: 0.9,
    }
    tweenArray.push(data);
}

function tween() {
    tweenArray.forEach((tweenObj) => {
        if (!tweenObj.isStopped) {
            if (tweenObj.current < tweenObj.max) {
                tweenObj.current = parseFloat((tweenObj.current + 0.01).toFixed(2));
            } else {
                tweenObj.current = parseFloat((tweenObj.current - 0.01).toFixed(2));
            }
            if (tweenObj.current === tweenObj.max) {
                [tweenObj.max, tweenObj.min] = [tweenObj.min, tweenObj.max];
            }
            tweenObj.sprite.scale.set(tweenObj.current);
        }
    });
}

wheel.prototype.start = function() {
    // console.log('click');

    // wheelsArray.forEach((start) => {
    this.isFinishing = false;
    this.isStoped = false;
    this.count = 0;
    // });

}

wheel.prototype.add = function(texture) {
    const sprite = new Sprite(texture);
    sprite.anchor.set(0.5);
    return sprite;
}

function stopTime(reel, time) {
    setTimeout(() => {
        wheelsArray[reel].isFinishing = true;
    }, time);
}

let backWin = [];

setTimeout(() => {
    backWin = [{
            paytlineId: 0,
            paydata: {
                ofKind: 3,
                data: {
                    reel0: 3,
                    reel1: 2,
                    reel2: 1,
                }
            }
        },
        {
            paytlineId: 1,
            paydata: {
                ofKind: 5,
                data: {
                    reel0: 3,
                    reel1: 3,
                    reel2: 3,
                    reel3: 3,
                    reel4: 3,
                }
            }
        },
        {
            paytlineId: 2,
            paydata: {
                ofKind: 5,
                data: {
                    reel0: 1,
                    reel1: 1,
                    reel2: 2,
                    reel3: 2,
                    reel4: 1
                }
            }
        },
        {
            paytlineId: 2,
            paydata: {
                ofKind: 5,
                data: {
                    reel0: 3,
                    reel1: 3,
                    reel2: 3,
                    reel3: 3,
                    reel4: 3
                }
            }
        },
    ];
}, 500);

let winAnimation = true;

function animationA() {
    if (!winAnimation) {
        return false;
    }
    backWin.forEach((wins) => {
        const data = Object.keys(backWin[i].paydata.data);
        data.forEach((reel) => {
            // this.animation(reel[reel.length - 1], wins.paydata.data[reel]);
            const s = wheelsArray[reel[reel.length - 1]].sprites[backWin[i].paydata.data[reel]];
            addTween(s);
        });

    });
    setTimeout(() => {
        deleteTween();
        animationA();
        i++;
        if (i === backWin.length) {
            i = 0;
        }
    }, 1000);
}

function deleteTween(data) {
    tweenArray = [];
}

wheel.prototype.tick = function() {

    if (this.isStoped === true && this.shifted === 0 && this.isFinishing) {
        return;
    }

    this.sprites.forEach((sprite) => {
        sprite.y += 10;
    });

    this.shifted += 10;
    if (this.shifted >= limit) {
        this.changeSYmbol();
    }
}


wheel.prototype.changeSYmbol = function() {
    this.wheelSprite.removeChild(this.sprites[this.sprites.length - 1]);
    this.finishAt = 4;
    this.sprites.pop();
    if (this.isFinishing) {
        this.texString = PIXI.utils.TextureCache[textArrayB['reel' + this.index][this.count]];
        this.data = this.add(this.texString);
        this.data.anchor.set(0.5);
        if (this.count === 3) {
            this.texString = PIXI.utils.TextureCache[texArray[randomNo(0, 9)]];
            this.data = this.add(this.texString);
            this.isStoped = true;
        }
        this.count++;
    } else {
        this.texString = PIXI.utils.TextureCache[texArray[randomNo(0, 9)]];
        this.data = this.add(this.texString);
    }
    this.data.position.set(160, 0);
    this.wheelSprite.addChild(this.data);
    this.sprites.unshift(this.data);
    this.shifted = 0;
    if (this.isStoped && this.index === 4) {
        winAnimation = true;
        animationA();
    }
}
/*to change the position
 * of sprite
 */
wheel.prototype.changePosition = function(sprite) {
    sprite.y = 300 * temp;
    sprite.x = 160;
    temp++;
}

wheel.prototype.changeSymbol = function() {

}

wheel.prototype.finish = function(wheelsArray) {
    this.isFinishing = true;
}

function spin(delta) {
    wheelsArray.forEach((wheel) => {
        wheel.tick();
    });
}